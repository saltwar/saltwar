# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/19 14:53:59 by fdexheim          #+#    #+#              #
#    Updated: 2016/09/26 10:51:07 by fdexheim         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = corewar
CC = gcc
SRC_PATH = ./src/
SRC_NAME =  main.c utils.c setup.c parse.c parse2.c clean.c display_memory.c \
			processes.c i_add.c i_aff.c i_and.c i_fork.c i_ld.c i_ldi.c \
			i_lfork.c i_live.c i_lld.c i_lldi.c i_or.c i_st.c i_sti.c i_sub.c \
			i_xor.c i_zjump.c inst_init.c game.c create_env.c visu.c utils2.c \
			visu2.c visu3.c utils3.c
CFLAGS = -Wall -Werror -Wextra -g
LIB = -L./libft/ -lft -lncurses
LIB_NAME = libft/libft.a
LIB_INC_PATH = libft/inc/
INC_PATH = ./inc/
OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "\033[33m$(NAME):\033[0m [\033[33mCompilation:\033[0m\033[37m $@\033[0m]"
	@$(CC) -o $@ -c $< $(CFLAGS) -I$(INC_PATH) -I$(LIB_INC_PATH)

$(NAME): $(LIB_NAME) $(OBJ)
	@$(CC) $(CFLAGS) -o $@ $(LIB) $^
	@echo "[\033[32m------------------------------------------\033[0m]"
	@echo "[\033[32m--------------- $(NAME) - OK -------------\033[0m]"
	@echo "[\033[32m------------------------------------------\033[0m]"

$(LIB_NAME):
	@make -C libft/ -j 8

clean:
	@rm -rf $(OBJ_PATH)
	@make clean -C libft/

fclean: clean
	@rm -f $(NAME)
	@make fclean -C libft/

re: fclean all
