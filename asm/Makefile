#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2016/01/29 15:20:46 by aaverty           #+#    #+#             *#
#*   Updated: 2016/04/29 17:06:54 by aaverty          ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

.PHONY: all clean fclean re

NAME = asm
CC = gcc
SRC_PATH = ./src/
SRC_NAME =  main.c asm_init.c asm_util.c asm_print_error.c asm_read_file.c \
            asm_parsing.c asm_write.c asm_parsing_header.c asm_check_line.c \
            asm_cursor.c asm_error_instr.c asm_error_label.c asm_error_param.c \
            asm_push_data.c asm_error_param_syntax.c asm_util_error.c \
            asm_error_param_syntax_ind.c asm_set_btcode.c

CFLAGS = -g -Wall -Wextra -Werror
LIB = -L../libft/ -lft
LIB_INC_PATH = ../libft/inc/
INC_PATH = ./inc/
OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
	@echo "\033[36m$(NAME):\033[0m [\033[35mCompilation:\033[0m\033[32m $@\033[0m]"
	@$(CC) -o $@ -c $< $(CFLAGS) -I$(INC_PATH) -I$(LIB_INC_PATH)

$(NAME): $(OBJ)
	@$(CC) $(CFLAGS) -o $@ $(LIB) $^
	@echo "[\033[36m------------------------------------------\033[0m]"
	@echo "[\033[36m----------------- $(NAME) - OK ---------------\033[0m]"
	@echo "[\033[36m------------------------------------------\033[0m]"

clean:
	@rm -rf $(OBJ_PATH)

fclean: clean
	@rm -f $(NAME)

re: fclean all

test : $(NAME)
	@make -C test