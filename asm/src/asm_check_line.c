/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_check_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/30 07:24:51 by aaverty           #+#    #+#             */
/*   Updated: 2016/08/30 07:09:50 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	asm_update_label_pos(t_app *app)
{
	t_label	*l;

	l = app->label;
	while (l)
	{
		if (l->pos == -1)
			l->pos = app->byte_count;
		l = l->next;
	}
}

void	asm_check_line(t_app *app, char *str, int line)
{
	t_btcode	*bt;

	bt = asm_create_bt();
	bt->n_line = line;
	str += asm_dodge_space_tab(str);
	if (!*str)
	{
		free(bt);
		return ;
	}
	asm_error_label(&str, app, line);
	asm_update_label_pos(app);
	if (!*str)
	{
		free(bt);
		return ;
	}
	asm_error_instr(&str, app, line, bt);
	asm_error_param(&str, line, bt);
	if (*str)
		ERROR("Error : too many parameters, line %d.\n", line);
	asm_push_bt(app, bt);
	app->byte_count += bt->cmd->cmd_size;
}
