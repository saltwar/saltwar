/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_parsing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/30 07:24:51 by aaverty           #+#    #+#             */
/*   Updated: 2016/08/30 07:09:50 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	asm_save_cmd(t_app *app)
{
	t_nodes		*n;

	n = app->lst_line;
	while (n && n->n_line + 1 < (int)app->cursor->line)
		n = n->next;
	while (n)
	{
		asm_check_line(app, n->line, n->n_line + 1);
		n = n->next;
	}
}

void	asm_delete_comment_after_header(t_app *app)
{
	t_nodes			*line;
	unsigned int	num_line;
	unsigned int	num_col;

	num_line = 1;
	line = app->lst_line;
	while (line)
	{
		num_col = 0;
		while (line->line[num_col])
		{
			if (num_line >= app->cursor->line
				&& (line->line[num_col] == '#'
					|| line->line[num_col] == ';'))
				line->line[num_col] = '\0';
			num_col++;
		}
		num_line++;
		line = line->next;
	}
}

void	asm_parse_header(t_app *app)
{
	char	c;

	app->header.magic = asm_reverse_uint(COREWAR_EXEC_MAGIC);
	while (1)
	{
		c = asm_read_char(app);
		if (c == ' ' || c == '\t' || c == '\n')
			continue ;
		else if (c == '#' || c == ';')
		{
			c = asm_read_char(app);
			while (c != '\n')
				c = asm_read_char(app);
		}
		else if (c == '.')
		{
			if (asm_read_directive(app))
				break ;
		}
		else
			ERROR("Error : unexpected character '%c', line %d, col %d.\n",
					c, app->cursor->line, app->cursor->col);
	}
}

void	asm_parse(t_app *app)
{
	asm_parse_header(app);
	asm_delete_comment_after_header(app);
	asm_save_cmd(app);
}
