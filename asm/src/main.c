/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 23:26:02 by aaverty           #+#    #+#             */
/*   Updated: 2016/08/30 15:04:25 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		main(int argc, char **argv)
{
	t_app app;

	if (argc != 2)
		ERROR("Error : wrong number of arguments.\n");
	asm_init_app(&app);
	asm_check_extension(&app, argv);
	asm_read_file(&app, argv[1]);
	asm_parse(&app);
	asm_open_out_file(&app, argv);
	if (app.byte_count == 0)
		ERROR("Error : the program has no instruction.\n");
	asm_write_data(&app);
	return (0);
}
