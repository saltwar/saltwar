/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 15:30:16 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/27 12:19:40 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H

# define COREWAR_H
# include "../libft/inc/libft.h"
# include "op.h"
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <ncurses.h>

typedef struct			s_process
{
	int					pc;
	int					pc_next;
	int					carry;
	unsigned int		reg[REG_NUMBER];
	int					live;
	int					inst;
	int					param_type[3];
	int					param[3];
	int					running;
	char				*name;
	int					player_number;
	int					moul_i;
	struct s_process	*next;
	struct s_process	*prev;
}						t_process;

typedef struct			s_inst
{
	int					optcode[16];
	int					nbr_cycle[16];
	int					octet[16];
	int					lab_size[16];
	int					size[16];
}						t_inst;

typedef struct			s_flags
{
	short int			ncurses;
	short int			a;
	short int			b;
	short int			v;
	short int			m;
	short int			pcmoves;
	short int			deaths;
	short int			operations;
	short int			cycles;
	short int			lives;
}						t_flags;

typedef struct			s_visu
{
	WINDOW				*n_mem;
	WINDOW				*n_text;
	WINDOW				*n_debug;
	int					total;
	int					kame_len;
}						t_visu;

typedef struct			s_env
{
	int					cycles;
	int					cycle_to_die;
	int					nbr_live;
	int					last_live;
	int					dump;
	int					octet[8];
	int					pnumber[4];
	int					psize[4];
	int					memstart[4];
	int					plives[4];
	int					pprocesses[4];
	int					is_dead[4];
	char				pnames[4][PROG_NAME_LENGTH + 1];
	char				pwarcry[4][COMMENT_LENGTH + 1];
	char				*paccess[4];
	int					pquantity;
	unsigned char		pcol[MEM_SIZE];
	unsigned char		mem_buff[MEM_SIZE];
	unsigned char		pcmap[MEM_SIZE];
	void				(*instructions[16])(struct s_env *, t_process *, int);
	int					i;
	int					j;
	struct s_process	*start_process;
	t_inst				instruction;
	t_flags				*flags;
	t_visu				*visu;
}						t_env;

unsigned int			swap_endians(unsigned int value);
unsigned char			*read_mem_size(t_env *env, int size, int pc);
void					kamehameha(t_env *env);
void					kamehameha_first_players(t_env *env, int i);
void					kamehameha_last_player(t_env *env, int i);
void					and_the_winner_is(t_env *env);
void					general_infos(t_env *env);
void					verbose_level(t_env *env, char **argv);
void					verbose_level2(t_env *env, int verbo);
void					who_is_alive(t_env *env);
void					corewar_help();
void					corewar_help2();
void					reset_pcmap_and_pprocesses(t_env *env);
void					live_bar(t_env *env, int i);
void					windowmaker(t_env *env);
void					introduce_in_order(t_env *env);
void					border_and_refresh(t_env *env);
void					write_players_in_text(t_env *env, int k);
void					write_window_text(t_env *env, int cycle);
void					write_window_mem(t_env *env, int cycle);
void					write_window_mem_2(t_env *env, int i);
void					check_flags(t_env *env, char **argv);
void					parse_exec(t_env *env, char **argv);
void					load_champs(t_env *env, int i, int fd);
void					load_in_memory(t_env *env, int i, int fd, int readturn);
void					get_name(int fd, int i, t_env *env);
void					get_size(int fd, int i, t_env *env);
void					get_warcry(int fd, int i, t_env *env);
void					parse_files(t_env *env);
void					ft_error(char *bug, t_env *env, void *free2,
							void *free3);
void					check_numbers_n(t_env *env, int number);
void					clear_dead_processes(t_env *env);
void					fill_numbers(t_env *env);
void					debug_parse(t_env *env);
void					debug_memory_buff(t_env *env);
void					debug_memory_buff_2(t_env *env, int i);
void					parse_with_n_flag(t_env *env, char **argv);
void					parse_dump(t_env *env, char **argv);
void					parse_without_n_flag(t_env *env, char **argv);
t_env					*create_env();
void					create_env_2(t_env *env);
void					create_env_3(t_env *env);
t_process				*new_process(t_env *env, int i, t_process *prev);
void					add_process(t_env *env, int i, t_process *start,
						int newpc);
void					free_processes(t_env *env);
void					create_players(t_env *env);
void					live_ghetto(t_env *env, t_process *process, int i);
void					ld_ghetto(t_env *env, t_process *process, int i);
void					st_ghetto(t_env *env, t_process *process, int i);
void					add_ghetto(t_env *env, t_process *process, int i);
void					sub_ghetto(t_env *env, t_process *process, int i);
void					and_ghetto(t_env *env, t_process *process, int i);
void					or_ghetto(t_env *env, t_process *process, int i);
void					xor_ghetto(t_env *env, t_process *process, int i);
void					zjump_ghetto(t_env *env, t_process *process, int i);
void					ldi_ghetto(t_env *env, t_process *process, int i);
void					sti_ghetto(t_env *env, t_process *process, int i);
void					fork_ghetto(t_env *env, t_process *process, int i);
void					lld_ghetto(t_env *env, t_process *process, int i);
void					lldi_ghetto(t_env *env, t_process *process, int i);
void					lfork_ghetto(t_env *env, t_process *process, int i);
void					aff_ghetto(t_env *env, t_process *process, int i);
void					inst_tab_init(t_env *env);
void					inst_tab_init2(t_env *env, int i);
void					ft_game(t_env *env, int cycle, int nbr_check);
void					find_inst(t_env *env, t_process *pro, int opt);
void					exec_inst(t_env *env, t_process *pro, int opt);
t_process				*batte_en_mousse(t_env *env, t_process *target);
int						verif_octet_codage(t_env *env, t_process *pro, int inst);
int						calc_move(t_env *env, t_process *pro, int max, int size);

#endif
