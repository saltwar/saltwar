/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:30:20 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/26 14:42:29 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			ft_error(char *bug, t_env *env, void *free2, void *free3)
{
	ft_printf("%s\n", bug);
	debug_memory_buff(env);
	if (env)
	{
		if (env->start_process)
			free_processes(env);
		if (env->flags)
			free(env->flags);
		if (env->visu)
		{
			endwin();
			free(env->visu);
		}
		free(env);
	}
	if (free2)
		free(free2);
	if (free3)
		free(free3);
	exit(0);
}
