/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:32:41 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/27 12:20:46 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		create_env_3(t_env *env)
{
	env->last_live = 0;
	env->cycles = 0;
	env->cycle_to_die = CYCLE_TO_DIE;
	env->nbr_live = 0;
	env->dump = 0;
	env->i = 0;
	env->j = 0;
	env->pquantity = 0;
}

void		create_env_2(t_env *env)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (i < 4)
	{
		while (j < PROG_NAME_LENGTH)
			env->pnames[i][j++] = '\0';
		j = 0;
		while (j < COMMENT_LENGTH)
			env->pwarcry[i][j++] = '\0';
		j = 0;
		env->memstart[i] = 0;
		env->pnumber[i] = -1;
		env->psize[i] = 0;
		env->is_dead[i] = 0;
		env->plives[i] = 0;
		env->paccess[i] = "\0";
		env->pprocesses[i] = 0;
		i++;
	}
}

void		create_struct_flags(t_env *env)
{
	env->flags = (t_flags *)malloc(sizeof(t_flags));
	if (!env->flags)
		ft_error("malloc yolo'ed", env, NULL, NULL);
	env->flags->ncurses = 0;
	env->flags->a = 0;
	env->flags->b = 0;
	env->flags->v = 0;
	env->flags->m = 0;
	env->flags->pcmoves = 0;
	env->flags->deaths = 0;
	env->flags->operations = 0;
	env->flags->cycles = 0;
	env->flags->lives = 0;
}

void		create_struct_visu(t_env *env)
{
	env->visu = (t_visu *)malloc(sizeof(t_visu));
	if (!env->visu)
		ft_error("malloc yolo'ed", env, NULL, NULL);
	env->visu->n_mem = NULL;
	env->visu->n_text = NULL;
	env->visu->n_debug = NULL;
	env->visu->total = 0;
	env->visu->kame_len = 0;
}


t_env		*create_env(void)
{
	t_env	*new;
	int		i;

	i = 0;
	new = (t_env *)malloc(sizeof(t_env));
	if (!new)
		return (NULL);
	while (i < MEM_SIZE)
	{
		new->mem_buff[i] = 0;
		new->pcmap[i] = 0;
		new->pcol[i] = 0;
		i++;
	}
	new->visu = NULL;
	new->flags = NULL;
	new->start_process = NULL;
	create_struct_visu(new);
	create_struct_flags(new);
	create_env_2(new);
	create_env_3(new);
	return (new);
}
