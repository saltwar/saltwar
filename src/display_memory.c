/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/25 10:10:37 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/12 10:57:26 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			debug_memory_buff_2(t_env *env, int i)
{
	if (env->pcol[i] == 1)
		ft_printf("\033[22;31m%.2x \033[0m", env->mem_buff[i]);
	else if (env->pcol[i] == 2)
		ft_printf("\033[22;34m%.2x \033[0m", env->mem_buff[i]);
	else if (env->pcol[i] == 3)
		ft_printf("\033[22;33m%.2x \033[0m", env->mem_buff[i]);
	else if (env->pcol[i] == 4)
		ft_printf("\033[22;32m%.2x \033[0m", env->mem_buff[i]);
	else
		ft_printf("%.2x ", env->mem_buff[i]);
}

void			debug_memory_buff(t_env *env)
{
	int			i;

	i = -1;
	ft_printf("\n|----------------- DEBUG MEMORY BUFF ------------------|\n\n");
	ft_printf("            ");
	while (i++ < 63)
		ft_printf("%2d ", i);
	i = 0;
	ft_printf("\n");
	while (i < MEM_SIZE)
	{
		if (i % 64 == 0)
			ft_printf("\n %8d   ", i);
		debug_memory_buff_2(env, i);
		i++;
	}
	ft_printf("\n\n|-------------- END DEBUG MEMORY BUFF ---------------|\n\n");
}
