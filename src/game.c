/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmoinat <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 12:57:06 by qmoinat           #+#    #+#             */
/*   Updated: 2016/09/25 16:09:11 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

static void		ft_octet(t_env *env, unsigned char octet)
{
	int			i;
	int			val;

	i = 0;
	val = 128;
	while (i <= 7)
	{
		if (octet >= val)
		{
			env->octet[i] = 1;
			octet -= val;
		}
		else
			env->octet[i] = 0;
		val /= 2;
		i++;
	}
}

static void		find_param(t_env *env, t_process *pro, int opt, int i)
{
	int			x;

	while (i++ < 2)
	{
		if ((x = (pro->param_type[i] == 1)))
			pro->param[i] = env->mem_buff[pro->pc_next % MEM_SIZE] - 1;
		else if (pro->param_type[i] == 2)
		{
		//	ft_printf("prems %.2x %.2x", env->mem_buff[pro->pc_next], env->mem_buff[pro->pc_next + 1]);
			pro->param[i] = (pro->pc + ((short)(256 * env->mem_buff[(pro->pc_next) % MEM_SIZE] + env->mem_buff[(pro->pc_next + 1) % MEM_SIZE]) % IDX_MOD));
			if (pro->param[i] < 0)
				pro->param[i] += MEM_SIZE;
			x = 2;
		}
		else if ((x = (pro->param_type[i] == 4)))
		{
			pro->param[i] = (ft_recursive_power(16, 6) * env->mem_buff[pro->pc_next % MEM_SIZE]
				+ ft_recursive_power(16, 4) * env->mem_buff[(pro->pc_next + 1) % MEM_SIZE] +
				ft_recursive_power(16, 2) * env->mem_buff[(pro->pc_next + 2) % MEM_SIZE] +
				env->mem_buff[(pro->pc_next + 3) % MEM_SIZE]);
			x = 4;
			if (env->instruction.lab_size[opt - 1] == 2)
			{
				pro->param[i] = (short)(256 * env->mem_buff[pro->pc_next % MEM_SIZE]
						+ env->mem_buff[(pro->pc_next + 1) % MEM_SIZE]);
				x = 2;
			}
		}
		pro->pc_next = (pro->pc_next + x) % MEM_SIZE;
	}
}

static void		find_inst_ocodage(t_env *env, t_process *pro, int opt, int i)
{
	int			x;

	x = opt;
	x = 0;
	ft_octet(env, env->mem_buff[(pro->pc + 1) % MEM_SIZE]);
	while (x <= 6 && i < 3)
	{
		if (env->octet[x] == 1)
		{
			if (env->octet[x + 1] == 1)
				pro->param_type[i] = 2;
			else
				pro->param_type[i] = 4;
		}
		else
		{
			if (env->octet[x + 1] == 1)
				pro->param_type[i] = 1;
			else
				pro->param_type[i] = 0;
		}
		x = x + 2;
		i++;
	}
}

void			find_inst(t_env *env, t_process *pro, int opt)
{
//		ft_printf("      champion = %15s pc = %4d, opt = %d\n", pro->name, pro->pc, opt);
	if (opt == 1 || opt == 9 || opt == 12 || opt == 15)
	{
		if (opt == 1 || opt == 12 || opt == 15)
			pro->param_type[0] = 4;
		else
			pro->param_type[0] = 2;
		pro->pc_next = (pro->pc + 1) % MEM_SIZE;
		pro->param_type[1] = 0;
		pro->param[1] = 0;
		pro->param_type[2] = 0;
		pro->param[2] = 0;
	}
	else
	{
		find_inst_ocodage(env, pro, opt, 0);
		pro->pc_next = (pro->pc + 2) % MEM_SIZE;
	}
	find_param(env, pro, opt, -1);
	env->j = verif_octet_codage(env, pro, opt);
	if (env->flags->operations && !env->flags->ncurses)
		ft_printf("champion: %s, inst: %2d, param 1 = %4d, param 2 = %4d, param 3 = %4d\n",
				pro->name, opt,  pro->param[0], pro->param[1], pro->param[2]);
	//	getchar();
	if (env->flags->ncurses)
		wprintw(env->visu->n_debug, "   champion: %s, inst: %2d, param 1 = %4d, param 2 = %4d, param 3 = %4d\n",
				pro->name, opt,  pro->param[0], pro->param[1], pro->param[2]);
}

void			exec_inst(t_env *env, t_process *pro, int opt)
{
	if (pro->running++ >= (env->instruction.nbr_cycle[opt - 1] - 1))
	{
		find_inst(env, pro, opt);
		if (env->j == 1)
			env->instructions[opt - 1](env, pro, pro->player_number);
		else
		{
			pro->pc_next = pro->pc + calc_move(env, pro, env->instruction.lab_size[opt - 1], env->instruction.size[opt - 1]);
		//	ft_printf("pc = %d, pc_next = %d, opt = %d, size = %d\n", pro->pc, pro->pc_next, opt, env->instruction.size[opt - 1]);
		//	debug_memory_buff(env);
		//	getchar();
		}
		pro->pc = pro->pc_next % MEM_SIZE;
		//	ft_printf("pc = %d\n\n", pro->pc);
		pro->inst = -1;
		pro->running = 0;
	}
}
