/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_add.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:29:19 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/05 15:02:59 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		add_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n add \n\n");
	i = env->i;
	if (process->param[0] >= 0 && process->param[0] < 16 &&
			process->param[1] >= 0 && process->param[1] < 16 &&
			process->param[2] >= 0 && process->param[2] < 16)
	{
//		ft_printf("debug : param0 r%d = %d, param1 r%d = %d => %d", process->param[0] + 1, process->reg[process->param[0]],
//				process->param[1] + 1, process->reg[process->param[1]], process->reg[process->param[1]] + process->reg[process->param[0]]);
		process->reg[process->param[2]] = process->reg[process->param[0]]
			+ process->reg[process->param[1]];
		if (process->reg[process->param[2]] == 0)
			process->carry = 1;
		else
			process->carry = 0;
	}
//	sleep(10);
}
