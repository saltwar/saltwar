/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_aff.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:21:41 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/05 15:03:07 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		aff_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n aff \n\n");
	int		disp;

	i = env->i;
	disp = process->reg[process->param[0]] % 256;
	ft_putchar(disp);
}
