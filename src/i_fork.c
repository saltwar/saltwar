/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_fork.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:20:32 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/25 13:17:10 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		fork_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n fork \n\n");
	int		newpc;

	newpc = process->pc + (process->param[0] % IDX_MOD);
	add_process(env, i, process, newpc);
//	debug_memory_buff(env);
//	getchar();
}
