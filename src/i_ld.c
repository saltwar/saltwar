/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_ld.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:28:17 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/22 15:48:25 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		ld_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n ld \n\n");
	int		tab[4];

	tab[0] = i;
	if (process->param[1] >= 0 && process->param[1] < 16)
	{
		if (process->param_type[0] == 2)
		{
			tab[3] = env->mem_buff[process->param[0]] * ft_recursive_power(16, 6);
			tab[2] = env->mem_buff[(process->param[0] + 1) % MEM_SIZE] * ft_recursive_power(16, 4);
			tab[1] = env->mem_buff[(process->param[0] + 2) % MEM_SIZE] * ft_recursive_power(16, 2);
			tab[0] = env->mem_buff[(process->param[0] + 3) % MEM_SIZE];
			process->reg[process->param[1]] = tab[0] + tab[1] + tab[2] + tab[3];
		}
		else if (process->param_type[0] == 4)
			process->reg[process->param[1]] = process->param[0];
		if (process->reg[process->param[1]] == 0)
			process->carry = 1;
		else
			process->carry = 0;
	}
//getchar();
}
