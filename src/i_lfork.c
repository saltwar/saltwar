/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_lfork.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:21:22 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/22 15:33:43 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		lfork_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n lfork \n\n");
	int		newpc;

	newpc = process->pc + process->param[0];
	add_process(env, i, process, newpc);
	if (newpc == 0)
		process->carry = 1;
	else
		process->carry = 0;
}
