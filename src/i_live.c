/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_live.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:27:36 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/26 14:36:58 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		live_ghetto(t_env *env, t_process *process, int i)
{
//	ft_printf("  live\n\n");
	i = (unsigned int)(ft_recursive_power(16, 8) - process->param[0]);
//	ft_printf("%d\n", ft_recursive_power(16, 7) - process->param[0]);
/*	if (i < 4 && i >= 0)
	{
//		ft_printf("i = %d pnumber i - 1 = %d\n", env->pnumber[i - 1]);
	}*/
	env->last_live = process->moul_i;
	if (env->flags->lives && !env->flags->ncurses)
		ft_printf("un processus dit que le joueur %s est en vie\n",
				env->pnames[process->moul_i]);
	if (env->flags->ncurses)
		env->plives[process->moul_i]++;
	env->nbr_live++;
	process->live = 1;
//	if (env->cycle_to_die < 1236)
//	{
//		ft_printf("nbr = %d, nbr_live = %d, pc = %d, %d\n", i, env->nbr_live, process->pc, process->pc_next);
//		getchar();
//	}
}
