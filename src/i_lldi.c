/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_lldi.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:21:06 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/26 13:53:17 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		lldi_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n lldi \n\n");
	int		somme;
	int		tab[4];

	somme = i;
//	ft_printf("debug : param0 = %d, param1 = %d, param2 = %d\n", process->param[0], process->reg[process->param[1]], process->param[2] + 1);
	if (process->param[2] >= 0 && process->param[2] < 16)
	{
		if (process->param_type[0] == 1)
			process->param[0] = process->reg[process->param[0]];
		else if (process->param_type[0] == 2)
			process->param[0] = process->pc + (short)(env->mem_buff[process->pc] * ft_recursive_power(16, 2)
				+ env->mem_buff[(process->pc + 1) % MEM_SIZE]);
		if (process->param_type[1] == 1)
			process->param[1] = process->reg[process->param[1]];
		somme = (process->pc + ((short)(process->param[0] + process->param[1]))) % MEM_SIZE;
//		ft_printf("somme = %d\n", somme);
		tab[3] = env->mem_buff[somme] * ft_recursive_power(16, 6);
//		ft_printf("tab[3] => %d | env->mem_buff => %d\n", tab[3], env->mem_buff[somme]);
		tab[2] = env->mem_buff[(somme + 1) % MEM_SIZE] * ft_recursive_power(16, 4);
//		ft_printf("tab[2] => %d | env->mem_buff => %d\n", tab[2], env->mem_buff[somme + 1] % MEM_SIZE);
		tab[1] = env->mem_buff[(somme + 2) % MEM_SIZE] * ft_recursive_power(16, 2);
//		ft_printf("tab[1] => %d | env->mem_buff => %d\n", tab[1], env->mem_buff[somme + 2] % MEM_SIZE);
		tab[0] = env->mem_buff[(somme + 3) % MEM_SIZE];
//		ft_printf("tab[0] => %d | env->mem_buff => %d\n", tab[0], env->mem_buff[somme + 3] % MEM_SIZE);
		somme = (unsigned int)(tab[0] + tab[1] + tab[2] + tab[3]);
		process->reg[process->param[2]] = somme;
//		ft_printf("somme = %ld\n", somme);
	}
	if (process->reg[process->param[2]] == 0)
		process->carry = 1;
	else
		process->carry = 0;
//	getchar();
}
