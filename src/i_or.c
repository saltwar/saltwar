/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_or.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:30:17 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/13 13:03:14 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		or_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n or \n\n");
	int		param0;
	int		param1;

	i = env->i;
	param0 = env->mem_buff[process->param[0]];
	param1 = env->mem_buff[process->param[1]];
//	ft_printf("debug : param0 r%d = %d, param1 r%d = %d => %d", process->param[0] + 1, process->reg[process->param[0]],
//			process->param[1] + 1, process->reg[process->param[1]], process->reg[process->param[1]] | process->reg[process->param[0]]);
	if (process->param_type[0] == 1)
		param0 = process->reg[process->param[0]];
	if (process->param_type[1] == 1)
		param1 = process->reg[process->param[1]];
	if (process->param_type[0] == 4)
		param0 = process->param[0];
	if (process->param_type[1] == 4)
		param1 = process->param[1];
	process->reg[process->param[2]] = param0 | param1;
	if (process->reg[process->param[2]] == 0)
		process->carry = 1;
	else
		process->carry = 0;
//getchar();
}
