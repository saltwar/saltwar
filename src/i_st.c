/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_st.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:28:55 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/16 14:57:33 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		st_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n st \n\n");
	int		tab[4];

	tab[0] = i;
	tab[0] = process->reg[process->param[0]] % ft_recursive_power(16, 2);
	tab[1] = (process->reg[process->param[0]] - tab[0]) % ft_recursive_power(16, 4);
	tab[2] = (process->reg[process->param[0]] - tab[1] - tab[0]) % ft_recursive_power(16, 6);
	tab[3] = (process->reg[process->param[0]] - tab[2] - tab[1] - tab[0]);
	if (process->param[0] < 16 && process->param[0] >= 0)
	{
		if (process->param_type[1] == 1)
		{
			process->reg[process->param[1]] = process->reg[process->param[0]];
//			ft_printf("debug : param0 r%d param1 r%d\n", process->param[0] + 1, process->param[1] + 1);
		}
		else
		{
			env->mem_buff[process->param[1]] = tab[3] / ft_recursive_power(16, 6);
			env->pcol[process->param[1]] = process->player_number + 1;
			env->mem_buff[(process->param[1] + 1) % MEM_SIZE] = tab[2] / ft_recursive_power(16, 4);
			env->pcol[(process->param[1] + 1) % MEM_SIZE] = process->player_number + 1;
			env->mem_buff[(process->param[1] + 2) % MEM_SIZE] = tab[1] / ft_recursive_power(16, 2);
			env->pcol[(process->param[1] + 2) % MEM_SIZE] = process->player_number + 1;
			env->mem_buff[(process->param[1] + 3) % MEM_SIZE] = tab[0];
			env->pcol[(process->param[1] + 3) % MEM_SIZE] = process->player_number + 1;
//			ft_printf("debug : param0 %d param1 %d\n", process->param[0] + 1, process->param[1]);
		}
	}
//	ft_printf("pc = %d\n", process->pc);
//	debug_memory_buff(env);
//	getchar();
}
