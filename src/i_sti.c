/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_sti.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 12:20:18 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/16 14:57:58 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		sti_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n sti \n\n");
	int		param1;
	int		param2;
	int		somme;
	int		tab[4];

	i = 0;
	param1 = process->param[1];
	param2 = process->param[2];
	if (process->param_type[1] == 1)
		param1 = process->reg[process->param[1]];
	else if (process->param_type[1] == 2)
		param1 = env->mem_buff[process->param[1]] * ft_recursive_power(16, 2)
			+ env->mem_buff[(process->param[1] + 1) % MEM_SIZE];
	if (process->param_type[2] == 1)
		param2 = process->reg[process->param[2]];
//	ft_printf("param1 = %d\n", param1);
//	ft_printf("param2 = %d\n", param2);
	somme = (process->pc + ((short)(param1 + param2) % IDX_MOD)) % MEM_SIZE;
	if (somme < 0)
		somme = MEM_SIZE + somme;
//	ft_printf("somme = %d\n", somme);
	if (process->param[0] >= 0 && process->param[0] < 16)
	{
//		ft_printf("somme = %d\n", process->reg[process->param[0]]);
		tab[0] = process->reg[process->param[0]] % ft_recursive_power(16, 2);
		tab[1] = (process->reg[process->param[0]] - tab[0]) % ft_recursive_power(16, 4);
		tab[2] = (process->reg[process->param[0]] - tab[1] - tab[0]) % ft_recursive_power(16, 6);
		tab[3] = (process->reg[process->param[0]] - tab[2] - tab[1] - tab[0]);

		env->mem_buff[somme] = tab[3] / ft_recursive_power(16, 6);
		env->pcol[somme] = process->player_number + 1;
		env->mem_buff[(somme + 1) % MEM_SIZE] = tab[2] / ft_recursive_power(16, 4);
		env->pcol[(somme + 1) % MEM_SIZE] = process->player_number + 1;
		env->mem_buff[(somme + 2) % MEM_SIZE] = tab[1] / ft_recursive_power(16, 2);
		env->pcol[(somme + 2) % MEM_SIZE] = process->player_number + 1;
		env->mem_buff[(somme + 3) % MEM_SIZE] = tab[0];
		env->pcol[(somme + 3) % MEM_SIZE] = process->player_number + 1;
	}
//	debug_memory_buff(env);
//	getchar();
}
