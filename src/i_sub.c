/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_sub.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:29:41 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/13 13:03:26 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		sub_ghetto(t_env *env, t_process *process, int i)
{
//ft_printf("\n sub \n\n");
	i = env->i;
	if (process->param[0] >= 0 && process->param[0] < 16 &&
			process->param[1] >= 0 && process->param[1] < 16 &&
			process->param[2] >= 0 && process->param[2] < 16)
	{
//		ft_printf("debug : param0 r%d = %d, param1 r%d = %d => %d", process->param[0] + 1, process->reg[process->param[0]],
//				process->param[1] + 1, process->reg[process->param[1]], process->reg[process->param[1]] - process->reg[process->param[0]]);
		process->reg[process->param[2]] = process->reg[process->param[0]]
			- process->reg[process->param[1]];
		if (process->reg[process->param[2]] == 0)
			process->carry = 1;
		else
			process->carry = 0;
	}
}
