/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inst_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qmoinat <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/12 12:45:18 by qmoinat           #+#    #+#             */
/*   Updated: 2016/09/26 13:55:23 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

static void		exec_game(t_env *env)
{
	int			i;
	t_process	*pro;

	i = 0;
	pro = env->start_process;
	while (pro)
	{
	//	pro->pc = pro->pc % MEM_SIZE;
		env->pprocesses[pro->moul_i]++;
		if (pro->inst == -1)
			pro->inst = env->mem_buff[pro->pc];
		env->pcmap[pro->pc] = 1;
		if (pro->inst <= 0 || pro->inst > 16)
		{
			pro->pc = (pro->pc + 1) % MEM_SIZE;
			pro->inst = -1;
			pro = pro->prev;
		}
		else
		{
			exec_inst(env, pro, pro->inst);
			pro = pro->prev;
		}
		i++;
	}
}

void			ft_game(t_env *env, int cycle, int nbr_check)
{
	int			i;
	t_process	*tmp;

	while (cycle <= env->cycle_to_die)
	{
		i = 0;
		exec_game(env);
		if (env->flags->cycles && !env->flags->ncurses)
			ft_printf("nbr_live : %d\n", env->nbr_live);
		if (cycle++ == env->cycle_to_die - 1)
		{
			nbr_check++;
			clear_dead_processes(env);
			if (env->flags->cycles && !env->flags->ncurses)
				ft_printf("cycle to die : %d, nbr_check = %d\n",
						env->cycle_to_die, nbr_check);
			if (env->nbr_live >= NBR_LIVE || nbr_check >= MAX_CHECKS)
			{
				env->cycle_to_die -= CYCLE_DELTA;
				nbr_check = 0;
			}
			tmp = env->start_process;
			while (tmp)
			{
				tmp->live = 0;
				i++;
				tmp = tmp->prev;
			}
			if (env->flags->cycles && !env->flags->ncurses)
				ft_printf("nbr pros : %d\n", i);
//			getchar();
			env->cycles = env->cycles + cycle;
			cycle = 0;
			env->nbr_live = 0;
		}
		if (env->flags->cycles && !env->flags->ncurses)
			ft_printf("nbr_cycle : %d Cycle to die : %d\n\n",
					env->cycles + cycle, env->cycle_to_die);
		if (env->cycles + cycle == 12978)
		{
			tmp = env->start_process;
			while (tmp)
			{
//				ft_printf("%d, %d\n", tmp->pc, tmp->running);
				tmp = tmp->prev;
			}
			if (env->flags->cycles && !env->flags->ncurses)
				debug_memory_buff(env);
//			getchar();
		}
		if (env->start_process == NULL)
			ft_error("FIN", env, NULL, NULL);
		if (env->flags->ncurses)
			write_window_mem(env, cycle);
		if (env->cycle_to_die < 0 || (env->cycles + cycle) == env->dump)
		{
			and_the_winner_is(env);
			ft_error("THE END", env, NULL, NULL);
		}
	}
}

static void		inst_tab_init3(t_env *env, int i)
{
	env->instruction.lab_size[i] = 0;
	if (i == 0 || i == 1 || i == 5 || i == 6 || i == 7 || i == 12)
		env->instruction.lab_size[i] = 4;
	else if (i == 8 || i == 9 || i == 10 || i == 11 || i == 13 || i == 14)
		env->instruction.lab_size[i] = 2;
	if (i == 0 || i == 8 || i == 11 || i == 14 || i == 15)
		env->instruction.size[i] = 1;
	else if (i == 1 || i == 2 || i == 12)
		env->instruction.size[i] = 2;
	else
		env->instruction.size[i] = 3;
}

void			inst_tab_init2(t_env *env, int i)
{
	env->instruction.optcode[i] = i + 1;
	if (i == 0 || i == 3 || i == 4 || i == 12)
		env->instruction.nbr_cycle[i] = 10;
	else if (i == 1 || i == 2)
		env->instruction.nbr_cycle[i] = 5;
	else if (i == 5 || i == 6 || i == 7)
		env->instruction.nbr_cycle[i] = 6;
	else if (i == 8)
		env->instruction.nbr_cycle[i] = 20;
	else if (i == 9 || i == 10)
		env->instruction.nbr_cycle[i] = 25;
	else if (i == 13)
		env->instruction.nbr_cycle[i] = 50;
	else if (i == 11)
		env->instruction.nbr_cycle[i] = 800;
	else if (i == 14)
		env->instruction.nbr_cycle[i] = 1000;
	else if (i == 15)
		env->instruction.nbr_cycle[i] = 2;
	if (i == 0 || i == 8 || i == 11 || i == 14)
		env->instruction.octet[i] = 0;
	else
		env->instruction.octet[i] = 1;
	inst_tab_init3(env, i);
}

void			inst_tab_init(t_env *env)
{
	env->instructions[0] = live_ghetto;
	env->instructions[1] = ld_ghetto;
	env->instructions[2] = st_ghetto;
	env->instructions[3] = add_ghetto;
	env->instructions[4] = sub_ghetto;
	env->instructions[5] = and_ghetto;
	env->instructions[6] = or_ghetto;
	env->instructions[7] = xor_ghetto;
	env->instructions[8] = zjump_ghetto;
	env->instructions[9] = ldi_ghetto;
	env->instructions[10] = sti_ghetto;
	env->instructions[11] = fork_ghetto;
	env->instructions[12] = lld_ghetto;
	env->instructions[13] = lldi_ghetto;
	env->instructions[14] = lfork_ghetto;
	env->instructions[15] = aff_ghetto;
}
