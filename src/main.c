/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 16:17:54 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/22 14:52:42 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void		check_numbers_n(t_env *env, int number)
{
	int		i;

	if (number < 0 || number > MAX_ARGS_NUMBER)
		ft_error("Parsing Error : Invalid Player Number", env, NULL, NULL);
	i = 0;
	while (env->pnumber[i])
	{
		if (number == env->pnumber[i])
		{
			ft_error("Parsing Error : Two Identical specified Player Numbers",
					env, NULL, NULL);
		}
		i++;
	}
}

void		fill_numbers(t_env *env)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 1;
	k = 0;
	while (i < env->pquantity)
	{
		if (env->pnumber[i] < 0)
		{
			while (k < env->pquantity)
			{
				if (j == env->pnumber[k])
				{
					j++;
					k = -1;
				}
				k++;
			}
			k = 0;
			env->pnumber[i] = j;
		}
		i++;
	}
}

void		parse_exec(t_env *env, char **argv)
{
	env->i = 1;
	env->pquantity = 0;
	while (argv[env->i])
	{
		if (argv[env->i][0] == '-')
			check_flags(env, argv);
		else
		{
			parse_without_n_flag(env, argv);
			env->pquantity++;
		}
		env->i++;
	}
	fill_numbers(env);
}

int			main(int argc, char **argv)
{
	t_env	*env;
	int		i;

	i = 0;
	env = NULL;
	if (argc < 2)
		corewar_help();
	env = create_env();
	if (!env)
		ft_error("Failed allocation memory for env", NULL, NULL, NULL);
	parse_exec(env, argv);
	parse_files(env);
	debug_parse(env);
	introduce_in_order(env);
	create_players(env);
	if (env->flags->ncurses)
		windowmaker(env);
	inst_tab_init(env);
	while (i < 16)
	{
		inst_tab_init2(env, i);
//		ft_printf("nbr_cycle: %d\n", env->instruction.nbr_cycle[i]);
		i++;
	}
	ft_game(env, 0, 0);
	if (env->visu)
	{
		wgetch(env->visu->n_debug);
		endwin();
	}
	free_processes(env);
	free(env);
	return (0);
}
