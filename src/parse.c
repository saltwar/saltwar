/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 11:09:44 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/12 15:06:45 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			get_name(int fd, int i, t_env *env)
{
	char		*chr;
	int			x;

	x = 0;
	chr = malloc(1);
	if (!chr)
		ft_error("Error Memory allocation failed\n", env, NULL, NULL);
	while (x < PROG_NAME_LENGTH && chr != 0)
	{
		read(fd, chr, 1);
		env->pnames[i][x] = chr[0];
		x++;
	}
	free(chr);
	if (ft_strlen(env->pnames[i]) > PROG_NAME_LENGTH)
		ft_error("Name too long", env, NULL, NULL);
}

void			parse_files(t_env *env)
{
	int			i;
	int			fd;
	uint32_t	*buff;

	i = 0;
	fd = 0;
	while (env->pnumber[i] != -1 && i < 4)
	{
		buff = malloc(sizeof(uint32_t));
		fd = open(env->paccess[i], O_RDONLY);
		if (fd == -1)
			ft_error("Error : Failed to open file", env, buff, NULL);
		read(fd, buff, 4);
		if (*buff != COREWAR_EXEC_MAGIC)
		{
			close(fd);
			ft_error("Invalid File format", env, buff, NULL);
		}
		free(buff);
		get_name(fd, i, env);
		get_size(fd, i, env);
		get_warcry(fd, i, env);
		load_champs(env, i, fd);
		i++;
	}
}

void			parse_dump(t_env *env, char **argv)
{
	char		*str;

	str = NULL;
	if (!argv[env->i + 1])
		ft_error("Parsing Error : No value after -dump flag", env, NULL, NULL);
	str = ft_itoa(ft_atoi(argv[env->i + 1]));
	if (ft_strcmp(str, argv[env->i + 1]))
		ft_error("Parsing error, invalid value for -dump flag", env, str, NULL);
	if (!ft_strcmp(str, argv[env->i + 1]) && ft_atoi(argv[env->i + 1]) > 0)
	{
		env->dump = ft_atoi(argv[env->i + 1]);
		env->i++;
	}
	free(str);
}

void			parse_with_n_flag(t_env *env, char **argv)
{
	char		*tmp;

	tmp = NULL;
	if (!argv[env->i + 1] || !argv[env->i + 2])
		ft_error("Parsing Error : Missing args after -n flag", env, NULL, NULL);
	if (env->pquantity >= 4)
		ft_error("Parsing Error : too many Players", env, NULL, NULL);
	if (argv[env->i + 1] && argv[env->i + 2] && !ft_strcmp(tmp = ft_itoa(ft_atoi(argv[env->i + 1])), argv[env->i + 1]) && ft_atoi(argv[env->i + 1]) > 0)
	{
		if (env->pquantity >= 4)
			ft_error("Parsing Error: too many Players", env, NULL, NULL);
		check_numbers_n(env, atoi(argv[env->i + 1]));
		env->paccess[env->j] = argv[env->i + 2];
		env->pnumber[env->j++] = atoi(argv[env->i + 1]);
		env->pquantity++;
		env->i += 2;
	}
	else
		ft_error("Flag -n present but Player Number incorrect", env, tmp, NULL);
	free(tmp);
}

void			parse_without_n_flag(t_env *env, char **argv)
{
	if (!ft_strcmp("-dump", argv[env->i]))
		ft_error("Parsing Error : wrong use of flags", env, NULL, NULL);
	if (env->pquantity >= 4)
		ft_error("Parsing Error : too many Players", env, NULL, NULL);
	env->paccess[env->j] = argv[env->i];
	env->j++;
}
