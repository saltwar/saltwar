/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/26 16:00:26 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/27 12:20:29 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			verbose_level2(t_env *env, int verbo)
{
	if (verbo >= 16)
	{
		env->flags->pcmoves = 1;
		verbo -= 16;
	}
	if (verbo >= 8)
	{
		env->flags->deaths = 1;
		verbo -= 8;
	}
	if (verbo >= 4)
	{
		env->flags->operations = 1;
		verbo -= 4;
	}
	if (verbo >= 2)
	{
		env->flags->cycles = 1;
		verbo -= 2;
	}
	if (verbo >= 1)
	{
		env->flags->lives = 1;
		verbo -= 1;
	}
}

void			verbose_level(t_env *env, char **argv)
{
	char		*verif;
	int			verbo;

	verif = NULL;
	env->flags->v = 1;
	if (!argv[env->i + 1])
		ft_error("Missing value after -v flag", env, NULL, NULL);
	verbo = ft_atoi(argv[env->i + 1]);
	verif = ft_itoa(verbo);
	if (ft_strcmp(verif, argv[env->i + 1]))
		ft_error("Missing value after -v flag", env, verif, NULL);
	free(verif);
	if (verbo)
	{
		verbose_level2(env, verbo);
	}
	env->i++;
}

void			check_flags(t_env *env, char **argv)
{
	int			j;
	t_flags		*tmp;

	tmp = env->flags;
	j = 1;
	if (!ft_strcmp(argv[env->i], "-dump"))
		parse_dump(env, argv);
	else if (!ft_strcmp(argv[env->i], "-n"))
		parse_with_n_flag(env, argv);
	else
	{
		tmp->ncurses = (!ft_strcmp(argv[env->i], "-ncurses")) ? 1 : tmp->ncurses;
		if (!ft_strcmp("-v", argv[env->i]))
			verbose_level(env, argv);
		else
		{
			while (argv[env->i][j])
			{
				tmp->a = (argv[env->i][j] == 'a') ? 1 : tmp->a;
				tmp->b = (argv[env->i][j] == 'b') ? 1 : tmp->b;
				tmp->m = (argv[env->i][j] == 'm') ? 1 : tmp->m;
				j++;
			}
		}
	}
}

void			get_warcry(int fd, int i, t_env *env)
{
	char		*chr;
	int			x;

	x = 0;
	chr = malloc(1);
	if (!chr)
		ft_error("malloc yolo'ed", env, NULL, NULL);
	lseek(fd, 0x8c, SEEK_SET);
	while (x < COMMENT_LENGTH && chr != 0)
	{
		read(fd, chr, 1);
		env->pwarcry[i][x] = chr[0];
		x++;
	}
	free(chr);
	if (ft_strlen(env->pwarcry[i]) > COMMENT_LENGTH)
		ft_error("Comment too long", env, NULL, NULL);
}

void			get_size(int fd, int i, t_env *env)
{
	uint32_t	*buff;

	buff = NULL;
	buff = malloc(sizeof(uint32_t));
	if (!buff)
		ft_error("malloc yolo'ed", env, NULL, NULL);
	lseek(fd, 0x88, SEEK_SET);
	read(fd, buff, 4);
	buff[0] = swap_endians(buff[0]);
	env->psize[i] = buff[0];
	free(buff);
	if (env->psize[i] > CHAMP_MAX_SIZE)
		ft_error("Champion too big", env, NULL, NULL);
}
