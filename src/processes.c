/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processes.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 10:58:21 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/25 15:23:44 by qmoinat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			clear_dead_processes(t_env *env)
{
	t_process	*tmp;

	tmp = env->start_process;
	while (tmp)
	{
		if (tmp->live == 0)
			tmp = batte_en_mousse(env, tmp);
		else
		{
			tmp->live = 0;
			tmp = tmp->prev;
		}
	}
	who_is_alive(env);
	env->plives[0] = 0;
	env->plives[1] = 0;
	env->plives[2] = 0;
	env->plives[3] = 0;
}

void			add_process(t_env *env, int i, t_process *pere, int newpc)
{
	int			j;
	t_process	*new;

//	ft_printf("add process, pc = %d\n", newpc);
	j = -1;
	new = NULL;
	new = (t_process *)malloc(sizeof(t_process));
	if (new)
	{
		new->pc = newpc;
		new->carry = pere->carry;
		new->name = env->pnames[i - 1];
		new->live = pere->live;
		new->inst = -1;
		new->running = 0;
		new->player_number = pere->player_number;
		new->moul_i = pere->moul_i;
		while (j++ < REG_NUMBER)
			new->reg[j] = pere->reg[j];
		j = -1;
		while (j++ < 3)
		{
			new->param_type[j] = 0;
			new->param[j] = 0;
		}
		new->next = NULL;
		new->prev = env->start_process;
		env->start_process->next = new;
		env->start_process = new;
	}
	else
		ft_error("Memory allocation Error on processes", env, new, NULL);
}

t_process		*new_process(t_env *env, int i, t_process *prev)
{
	int			j;
	t_process	*new;

//	ft_printf("new process, i = %d\n", i);
	j = 0;
	new = NULL;
	new = (t_process *)malloc(sizeof(t_process));
	if (new)
	{
		new->pc = env->memstart[i];
		new->carry = 0;
		new->name = env->pnames[i];
		new->live = 0;
		new->running = 0;
		new->inst = -1;
		new->player_number = env->pnumber[i];
		new->moul_i = i;
		new->reg[j] = (unsigned int)(ft_recursive_power(16, 8) - new->player_number);
		while (j++ < REG_NUMBER)
			new->reg[j] = 0;
		j = -1;
		while (j++ < 3)
		{
			new->param_type[j] = 0;
			new->param[j] = 0;
		}
		new->next = NULL;
		new->prev = prev;
		if (new->prev != NULL)
			new->prev->next = new;
	}
	else
		ft_error("Memory allocation Error on processes", env, new, NULL);
	return (new);
}

void			free_processes(t_env *env)
{
	t_process	*to_free;
	t_process	*tmp;

	to_free = NULL;
	tmp = env->start_process;
	while (tmp)
	{
		to_free = tmp;
		tmp = tmp->prev;
		if (to_free)
			free(to_free);
	}
	free(tmp);
}

t_process		*batte_en_mousse(t_env *env, t_process *target)
{
	t_process	*tmp;
	t_process	*tmp2;

	tmp = NULL;
	tmp = target;
	tmp2 = NULL;
	if (target->prev)
	{
		target->prev->next = tmp->next;
		tmp2 = target->prev;
	}
	if (target->next)
		target->next->prev = tmp->prev;
	else
		env->start_process = target->prev;
//	write(1, "\a", 1);
	free(target);
	tmp = env->start_process;
	while (tmp)
		tmp = tmp->prev;
	return (tmp2);
}
