/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 13:54:40 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/21 14:00:06 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void				load_in_memory(t_env *env, int fd, int i, int readturn)
{
	unsigned char	*value;

	value = malloc(sizeof(unsigned char));
	if (!value)
		ft_error("Memory allocation Error", env, NULL, NULL);
	while (readturn != 0)
	{
		readturn = read(fd, value, 1);
		if (readturn != 0)
		{
			env->mem_buff[env->i] = value[0];
			env->pcol[env->i] = env->pnumber[i];
		}
		env->j++;
		env->i++;
	}
	free(value);
}

void				load_champs(t_env *env, int i, int fd)
{
	int				readturn;

	readturn = 1;
	env->j = 0;
	env->i = (i) * (MEM_SIZE) / env->pquantity;
	env->memstart[i] = (i) * (MEM_SIZE) / env->pquantity;
	lseek(fd, 0x0000890, SEEK_SET);
	load_in_memory(env, fd, i, readturn);
	close(fd);
}

void				create_players(t_env *env)
{
	int				i;
	t_process		*prev;

	i = 0;
	prev = NULL;
	while (i < 4 && env-> pnumber[i] != -1)
	{
		env->start_process = new_process(env, i, prev);
		prev = env->start_process;
		env->is_dead[i] = 0;
		i++;
	}
}
