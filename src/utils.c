/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 13:43:01 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/27 12:22:50 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

unsigned int		swap_endians(unsigned int value)
{
	unsigned int	new_value;
	unsigned int	values[4];
	int				i;

	i = -1;
	new_value = 0;
	while (i++ < 4)
		values[i] = 0;
	i = 0;
	values[0] = (value & 0x000000ff) << 24u;
	values[1] = (value & 0x0000ff00) << 8u;
	values[2] = (value & 0x00ff0000) >> 8u;
	values[3] = (value & 0xff000000) >> 24u;
	new_value = values[0] | values[1] | values[2] | values[3];
	return (new_value);
}

unsigned char		*read_mem_size(t_env *env, int size, int pc)
{
	unsigned char	*sum;
	char			*tmp;
	char			tmp2;

	tmp = NULL;
	sum = NULL;
	ft_memcpy(sum, &env->mem_buff[pc], 1);
	while (pc < (pc + size))
	{
		tmp2 = (char)env->mem_buff[pc];
		tmp = ft_strjoin((char*)sum, &tmp2);
		pc++;
	}
	return ((unsigned char*)tmp);
}

void				debug_parse(t_env *env)
{
	int				k;

	k = 0;
	ft_printf("\n ------ FLAGS ------ \n\n ncurses = %hd\n a = %hd\n b = %hd\n v = %hd\n m = %hd\n dump = %d\n\n",
			env->flags->ncurses, env->flags->a, env->flags->b,
			env->flags->v, env->flags->m, env->dump);
	ft_printf("pcmoves = %hd\n deaths = %hd\n operations = %hd\n cycles = %hd\n lives = %hd\n\n -------------------\n\n",
			env->flags->pcmoves, env->flags->deaths, env->flags->operations,
			env->flags->cycles, env->flags->lives);
}

int		calc_move(t_env *env, t_process *pro, int max, int size)
{
	int				rt;
	int				i;

	i = 0;
	rt = 2;
	while (i < size)
	{
		if (((env->mem_buff[pro->pc + 1] << (i * 2)) & 0xc0) == 0x40)
			rt += 1;
		else if (((env->mem_buff[pro->pc + 1] << (i * 2)) & 0xc0) == 0x80)
			rt += max;
		else if (((env->mem_buff[pro->pc + 1] << (i * 2)) & 0xc0) == 0xc0)
			rt += 2;
		i++;
	}
	return (rt);
}

int					verif_octet_codage(t_env *env, t_process *pro, int inst)
{
	int		i;

	i = 0;
	env->i = 0;
//	ft_printf("%.2x %.2x\n", env->mem_buff[pro->pc], env->mem_buff[pro->pc + 1]);
//	ft_printf("%d %d %d\n", pro->param_type[0], pro->param_type[1], pro->param_type[2]);
	if (inst == 1 || inst == 9 || inst == 12 || inst == 15)
		return (1);
	if (pro->param_type[0] == 0)
		return (0);
	else
	{
		if (inst == 16 && pro->param_type[0] != 1)
			return (0);
		else if ((inst == 4 || inst == 5) && (pro->param_type[0] != 1 ||
					pro->param_type[1] != 1 || pro->param_type[2] != 1))
			return (0);
		else
		{
			if (inst == 2 || inst == 6 || inst == 7 || inst == 8 || inst == 10
					|| inst == 13 || inst == 14)
			{
				if ((inst == 2 || inst == 13) && (pro->param_type[0] == 0 ||
							pro->param_type[0] == 1 || pro->param_type[1] != 1))
					return (0);
				else if ((inst == 10 || inst == 14) && (pro->param_type[1] == 0 ||
							pro->param_type[1] == 2 || pro->param_type[2] != 1
							|| pro->param_type[0] == 0))
					return (0);
				else if (inst == 6 || inst == 7 || inst == 8)
				{
					if (pro->param_type[0] == 0 || pro->param_type[1] == 0 || pro->param_type[2] != 1)
						return(0);
				}
			}
			else
			{
				if (pro->param_type[0] != 1)
					return (0);
				else
				{
					if (inst == 11 && (pro->param_type[1] == 0 ||
								pro->param_type[2] == 0 || pro->param_type[2] == 2))
						return (0);
					else if (inst == 3 && (pro->param_type[1] == 0
								|| pro->param_type[1] == 4))
						return (0);
				}
			}
		}
	}
	return(1);
}
