/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 13:37:47 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/22 13:17:25 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			corewar_help2()
{

}

void			corewar_help()
{
	ft_printf("\n You there ! Let me bend your ear for a moment !\n\n"
	" Fighting is a necessity here in Wraeclast. It comes straight "
	"after breathing\n and right before eating on the 'how do I "
	"keep myself alive today?' list.\n\n"
	" Which reminds me, any time you feel like a little "
	"pre-breakfast tussle,\n come and find me. I'll be happy "
	"to kick the sleep out of your eyes.\n\n"
	"----------------------------------------------------------------------\n"
	"                                  Flags                       \n"
	"----------------------------------------------------------------------\n"
	"-n [champion.cor] : Sets a number to a champion (1-4)\n"
	"-dump [value]     : stops after [value] cycles and dumps"
	" memory \n-ncurses          : enables ncurses mod for some"
	" shiny shit on screen like Zaz does\n"
	"-a -b             : yet unused flags that will be given use "
	"for those sweet bonus points\n"
	"-v [number]       : Verbose level (Will be ignored if ncurses"
	" mod is on)\nIt works just like Zaz's Corewar by adding values"
	" :\n\t\t0 -> Default\n\t\t1 -> lives\n\t\t2 -> cycles\n"
	"\t\t4 -> Operations\n"
	"\t\t8 -> champion deaths \n\t\t16 -> processes moves\n"
	"----------------------------------------------------------------------\n");
	exit(0);
}

void			who_is_alive(t_env *env)
{
	int			i;

	i = 0;
	while (i < 4)
	{
		if (!env->plives[i])
			env->is_dead[i] = 1;
		i++;
	}
}

void			reset_pcmap_and_pprocesses(t_env *env)
{
	int			i;

	i = 0;
	while (i < MEM_SIZE)
	{
		env->pcmap[i] = 0;
		i++;
	}
	i = 0;
	while (i < 4)
	{
		env->pprocesses[i] = 0;
		i++;
	}
	if (env->flags->ncurses)
	{
		wmove(env->visu->n_debug, 0, 3);
		wprintw(env->visu->n_debug, "\n\n");
	}
}

void		introduce_in_order(t_env *env)
{
	int		i;
	int		k;
	int		j;

	i = 0;
	k = 0;
	j = 1;
	ft_printf("Introducing contestants...\n");
	while (i < env->pquantity)
	{
		k = 0;
		while (j != env->pnumber[k] && k < env->pquantity)
			k++;
		if (env->pnumber[k] != -1)
			ft_printf("* Player %d, weighing %3d, bytes, \"%s\", (\"%s\") !\n",
					env->pnumber[k], env->psize[k], env->pnames[k],
					env->pwarcry[k]);
		j++;
		i++;
	}
}
