/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 14:53:06 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/21 16:15:46 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			and_the_winner_is(t_env *env)
{
	if (!env->flags->ncurses)
		ft_printf("Contestant %d, \"%s\", has won !\n",
				env->pnumber[env->last_live], env->pnames[env->last_live]);
}
