/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 13:07:40 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/22 15:56:07 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			windowmaker(t_env *env)
{
	initscr();
	start_color();
	env->visu->n_mem = newwin(68, 200, 0, 0);
	env->visu->n_text = newwin(34, 120, 0, 199);
	env->visu->n_debug = newwin(41, 120, 33, 199);
	init_color(10, 400, 400, 400);
	init_color(11, 765, 240, 240);
	init_color(12, 153, 306, 765);
	init_color(13, 765, 765, 306);
	init_color(14, 0, 612, 306);
	init_pair(0, 10, COLOR_BLACK);
	init_pair(1, 11, COLOR_BLACK);
	init_pair(2, 12, COLOR_BLACK);
	init_pair(3, 13, COLOR_BLACK);
	init_pair(4, 14, COLOR_BLACK);
	init_pair(5, 10, COLOR_BLACK);
	init_pair(10, COLOR_BLACK, 10);
	init_pair(11, COLOR_BLACK, 11);
	init_pair(12, COLOR_BLACK, 12);
	init_pair(13, COLOR_BLACK, 13);
	init_pair(14, COLOR_BLACK, 14);
}

void			border_and_refresh(t_env *env)
{
	wattron(env->visu->n_mem, COLOR_PAIR(10));
	wattron(env->visu->n_text, COLOR_PAIR(10));
	wattron(env->visu->n_debug, COLOR_PAIR(10));
	wborder(env->visu->n_mem, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wborder(env->visu->n_text, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wborder(env->visu->n_debug, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wattroff(env->visu->n_mem, COLOR_PAIR(10));
	wattroff(env->visu->n_text, COLOR_PAIR(10));
	wattroff(env->visu->n_debug, COLOR_PAIR(10));
	wnoutrefresh(env->visu->n_mem);
	wnoutrefresh(env->visu->n_text);
	wnoutrefresh(env->visu->n_debug);
	doupdate();
}

void			write_window_mem_2(t_env *env, int i)
{
	if (env->pcmap[i])
	{
		wattron(env->visu->n_mem, COLOR_PAIR(env->pcol[i] + 10));
		wprintw(env->visu->n_mem, "%.2x", env->mem_buff[i]);
		wattroff(env->visu->n_mem, COLOR_PAIR(env->pcol[i] + 10));
		wprintw(env->visu->n_mem, " ", env->mem_buff[i]);
	}
	else
	{
		wattron(env->visu->n_mem, COLOR_PAIR(env->pcol[i]));
		wprintw(env->visu->n_mem, "%.2x ", env->mem_buff[i]);
		wattroff(env->visu->n_mem, COLOR_PAIR(env->pcol[i]));
	}
}

void			write_window_mem(t_env *env, int cycle)
{
	int			i;

	i = -1;
	wmove(env->visu->n_mem, 1, 3);
	wattron(env->visu->n_mem, COLOR_PAIR(0));
	while (i < MEM_SIZE)
	{
		if (i % 64 == 0)
			wprintw(env->visu->n_mem, "\n    ");
		write_window_mem_2(env, i);
		i++;
	}
	write_window_text(env, cycle);
//	if (env->cycles + cycle >= 2500)
//		wgetch(env->visu->n_debug);
	border_and_refresh(env);
	reset_pcmap_and_pprocesses(env);
}
