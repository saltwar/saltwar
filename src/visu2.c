/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 14:08:33 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/26 14:43:16 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"


void			live_bar(t_env *env, int i)
{
	wprintw(env->visu->n_text, "    ");
	if (env->plives[i])
	{
		wattron(env->visu->n_text, COLOR_PAIR(env->pnumber[i]));
		wprintw(env->visu->n_text, "------------------------------------------"
		"-------------------------------------------------------------\n    ");
		wattroff(env->visu->n_text, COLOR_PAIR(env->pnumber[i]));
		wprintw(env->visu->n_text, "Lives in this period : %d\n    ",
				env->plives[i]);
	}
	else
	{
		wprintw(env->visu->n_text, "------------------------------------------"
		"-------------------------------------------------------------\n    ");
		wprintw(env->visu->n_text, "Lives in this period : %d\n    ",
				env->plives[i]);
	}
	wprintw(env->visu->n_text, "Processes : %d\n", env->pprocesses[i]);
	wprintw(env->visu->n_text, "\n\n");
}

void			write_players_in_text(t_env *env, int k)
{
	wprintw(env->visu->n_text, "    Player %d : ", env->pnumber[k]);
	wattron(env->visu->n_text, COLOR_PAIR(env->pnumber[k]));
	wprintw(env->visu->n_text, "%s", env->pnames[k]);
	wattroff(env->visu->n_text, COLOR_PAIR(env->pnumber[k]));
	if (env->is_dead[k])
		wprintw(env->visu->n_text, " - DEAD\n");
	else
		wprintw(env->visu->n_text, "       \n");

}

void			general_infos(t_env *env)
{
	if (env->cycle_to_die < 0)
		wprintw(env->visu->n_text, "\n    And the Winner is : ");
	else
		wprintw(env->visu->n_text, "\n    Last live : ");
	wattron(env->visu->n_text, COLOR_PAIR(env->pnumber[env->last_live]));
	wprintw(env->visu->n_text, "%s\n    ", env->pnames[env->last_live]);
	wattroff(env->visu->n_text, COLOR_PAIR(env->pnumber[env->last_live]));
	if (env->cycle_to_die < 0)
	{
		border_and_refresh(env);
		wgetch(env->visu->n_debug);
		ft_error("DONE", env, NULL, NULL);
	}
}

void			write_window_text(t_env *env, int cycle)
{
	int			i;
	int			j;
	int			k;

	i = 0;
	j = 1;
	k = 0;
	wmove(env->visu->n_text, 0, 0);
	wprintw(env->visu->n_text, "\n\n\t\t\t\t\t\t -- COREWAR --\n");
	while (i < 4)
	{
		k = 0;
		while (j != env->pnumber[k] && k < env->pquantity)
			k++;
		if (env->pnumber[k] != -1)
		{
			write_players_in_text(env, k);
			live_bar(env, k);
		}
		j++;
		i++;
	}
	wmove(env->visu->n_text, 25, 0);
	wprintw(env->visu->n_text, "\n    Cycles : %d\n    Cycle_to_die : %d\n",
			env->cycles + cycle, env->cycle_to_die);
	general_infos(env);
	kamehameha(env);
}
