/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu3.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdexheim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 10:48:01 by fdexheim          #+#    #+#             */
/*   Updated: 2016/09/26 16:07:42 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/corewar.h"

void			kamehameha(t_env *env)
{
	int			i;
	int			j;

	env->visu->kame_len = 0;
	wmove(env->visu->n_text, 31, 4);
//	wclrtoeol(env->visu->n_text);
	wprintw(env->visu->n_text, "KWAMEHAME-BAR : ");
	i = 0;
	j = 0;
	env->visu->total = env->plives[0] + env->plives[1]
		+ env->plives[2] + env->plives[3];
	while (i < 4)
	{
		if (env->plives[i] && i < env->pquantity - 1)
			kamehameha_first_players(env, i);
		else if (env->plives[i] && i == env->pquantity - 1)
			kamehameha_last_player(env, i);
		j = 0;
		i++;
	}
	wprintw(env->visu->n_text, "    ");
}

void			kamehameha_first_players(t_env *env, int i)
{
	int			j;
	int			k;

	j = 0;
	k = (80 * env->plives[i] / env->visu->total);
	wattron(env->visu->n_text, COLOR_PAIR(env->pnumber[i] + 10));
	wprintw(env->visu->n_text, " P%d ", env->pnumber[i]);
	while (j < k)
	{
		wprintw(env->visu->n_text, "-");
		j++;
	}
	env->visu->kame_len += j;
	wprintw(env->visu->n_text, ">");
	wattroff(env->visu->n_text, COLOR_PAIR(env->pnumber[i] + 10));
}

void			kamehameha_last_player(t_env *env, int i)
{
	int			j;
	int			k;
	int			l;

	l = 0;
	j = 0;
	k = 80 - env->visu->kame_len;
	wattron(env->visu->n_text, COLOR_PAIR(env->pnumber[i] + 10));
	wprintw(env->visu->n_text, "<");
	while (j < k)
	{
		wprintw(env->visu->n_text, "-");
		j++;
	}
	wprintw(env->visu->n_text, " P%d ", env->pnumber[i]);
	wattroff(env->visu->n_text, COLOR_PAIR(env->pnumber[i] + 10));
}
